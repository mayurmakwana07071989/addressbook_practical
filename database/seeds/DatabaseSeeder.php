<?php

use Illuminate\Database\Seeder;
use App\Cities;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Cities::create(["city_name"=>'Ahmedabad']);
        Cities::create(["city_name"=>'Rajkot']);
        Cities::create(["city_name"=>'Surendranagar']);
        Cities::create(["city_name"=>'Jamnagar']);
        Cities::create(["city_name"=>'Amreli']);
    }
}
