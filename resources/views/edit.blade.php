<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
       
            <div class="content">
            <form action="{{ route('updateAddress',['id'=>$adb->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                FirstName<input type="text" name="firstname" value="{{ $adb->firstname }}" required=""><br>
                Lastname<input type="text" name="lastname" value="{{ $adb->lastname }}" required=""><br>
                <img src="{{ url('uploads/'.$adb->profile_pic) }}" style="height:100px;width:100px" /><br>
                Profile <input type="file" name="profile_pic"><br>
                Email <input type="email" name="email" value="{{ $adb->email }}" required=""><br>
                Street <input type="text" name="street" value="{{ $adb->street }}" required=""><br>
                Zipcode <input type="text" name="zipcode" value="{{ $adb->zipcode }}" required=""><br>
                City <select name="city_id" required="">
                    @foreach($city as $c)
                        @if($adb->city_id==$c->id)

                        <option value="{{ $c->id }}" selected="">{{ $c->city_name }}</option>
                        @else
                        <option value="{{ $c->id }}">{{ $c->city_name }}</option>
                        @endif
                    @endforeach
                     </select>
                     <br><br>
                    <input type="submit" name="addnewsubmit">
                    <input type="reset" name="reset">
                    <a style="text-align: left;" href="{{ route('addressbook') }}">Back</a> 
            </form>
            </div>
        
    </body>
</html>
