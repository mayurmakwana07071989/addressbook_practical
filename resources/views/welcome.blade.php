<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        
            <div class="content">
            <a style="text-align: left;" href="{{ route('addnew') }}">Add new address</a>   <center>
               <div> <a href="{{ route('exportJson') }}">Export Json </a></div>
               <div><a href="{{ route('exportXml') }}">Export Xml </a></div>
                
            <table border="1">
                <tr>
                    <td>Firstname</td>
                    <td>Lastname</td>
                    <td>Profile-pic</td>
                    <td>Email</td>
                    <td>Street</td>
                    <td>Zipcode</td>
                    <td>City</td>
                    <td>Action</td>
                </tr>

                @foreach($adb as $data)
                 <tr>
                    <td>{{ $data->firstname }}</td>
                    <td>{{ $data->lastname }}</td>
                    <td><img src="{{ url('uploads/'.$data->profile_pic) }}" style="height:100px;width:100px" /></td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->street }}</td>
                    <td>{{ $data->zipcode }}</td>
                    <td>{{ \App\Cities::find($data->city_id)->city_name }}</td>
                    <td> <a href="{{ route('edit',['id'=>$data->id]) }}">Edit</a></td>
                </tr>
                @endforeach    

                
            </table>
            </center> 
            </div>
            
    </body>
</html>
