<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AddressbookController@index')->name('addressbook');
Route::get('/addnew', 'AddressbookController@addnew')->name('addnew');
Route::post('/saveAddress', 'AddressbookController@saveAddress')->name('saveAddress');
Route::get('/edit/{id}', 'AddressbookController@edit')->name('edit');
Route::post('/updateAddress/{id}', 'AddressbookController@updateAddress')->name('updateAddress');

Route::get('/exportJson', 'AddressbookController@exportJson')->name('exportJson');
Route::get('/exportXml', 'AddressbookController@exportXml')->name('exportXml');
