<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cities;
use App\Addressbook;
use View;
use File;
use Response;
use Storage;
class AddressbookController extends Controller
{

    public function index(){
    	$adb = Addressbook::get();
    	return view('welcome',compact('adb'));
    }

    public function addnew(){
    	$city = Cities::get();

    	return view('addnew',compact('city'));
    }

   	public function saveAddress(Request $request){
   		$file = $request->file('profile_pic');
   		$destinationPath = 'uploads';
      	$file->move($destinationPath,$file->getClientOriginalName());

   		Addressbook::create(['firstname'=>$request->firstname,'lastname'=>$request->lastname,'profile_pic'=>$file->getClientOriginalName(),'email'=>$request->email,'street'=>$request->street,'zipcode'=>$request->zipcode,'city_id'=>$request->city_id]);

   		return redirect('/');
   	}

   	public function edit($id){
    	$city = Cities::get();
		$adb = Addressbook::find($id);

    	return view('edit',compact('city','adb'));
    }

   	public function updateAddress($id,Request $request){

   		$olddata = Addressbook::find($id);
   		if($request->file('profile_pic'))
   		{
	   		$file = $request->file('profile_pic');
	   		$destinationPath = 'uploads';
	      	$file->move($destinationPath,$file->getClientOriginalName());
	      	$updatefile = $file->getClientOriginalName();
		}else{
			$updatefile =$olddata->profile_pic;
		}
   		Addressbook::where('id',$id)->update(['firstname'=>$request->firstname,'lastname'=>$request->lastname,'profile_pic'=>$updatefile,'email'=>$request->email,'street'=>$request->street,'zipcode'=>$request->zipcode,'city_id'=>$request->city_id]);

   		return redirect('/');
   	}

   	public function exportJson(){

   		$adb = Addressbook::get();
   		$data = json_encode($adb);
   		//dd($data);
   		Storage::put('file.txt', 'Your name');
	  	$fileName = 'datafile.json';
		File::put('uploads/json/'.$fileName,$data);
	  	return Response::download('uploads/json/'.$fileName);
   	}

   	public function exportXml(){
   		$adb = Addressbook::get()->toArray();


		$xmlobj = new \SimpleXMLElement("<root></root>");
		$xmldoc =$this->convert($adb,$xmlobj);
		$xmldoc->asXML('uploads/xml/dataxml.xml');
		return Response::download('uploads/xml/dataxml.xml');

   	}



 
function convert($array, $xml){
    foreach($array  as $key=>$line){
        if(!is_array($line)){
            $data = $xml->addChild($key, $line);

        }else{
            $obj = $xml->addChild($key);

            if(!empty($line['attribute'])){

                $attr = explode(":",$line['attribute']);
                $obj->addAttribute($attr[0],$attr[1]);
                unset($line['attribute']);
            }
            $this->convert($line, $obj);
        }
    }
    return $xml;
}

}



