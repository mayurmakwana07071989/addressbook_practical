<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addressbook extends Model
{
     protected $table = 'addressbooks';
     protected $guarded = [];
}
